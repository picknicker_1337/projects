﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetzwerkplanCreator
{
    class Repository
    {
        static public void Read(string inputPath)
        {
            // neuees Netzwerk
            Network network = new Network();
            
            #region daten auslesen und validieren
            // datei existiert
            if (File.Exists(inputPath))
            {

                // datei lesen
                using (StreamReader inputString = new StreamReader(inputPath))
                {

                    // deklaration
                    string line;
                    string[] splitLine;
                    int lineCounter = 1;

                    // head line
                    if ((line = inputString.ReadLine()) == "Vorgang;Beschreibung;Dauer;Vorgänger")
                    {
                        // line ungleich null
                        while ((line = inputString.ReadLine()) != null)
                        {

                            if (line.Contains(';'))
                            {
                                // first split
                                splitLine = line.Split(';');

                                // line hat 4 einträge
                                if (splitLine.Length == 4)
                                {

                                    // netzwerk knoten erstellen
                                    NetworkEntry neuerKnoten = new NetworkEntry(
                                        splitLine[0]
                                        , splitLine[1]
                                        , Convert.ToInt32(splitLine[2]));

                                    // knoten hat vorgänger
                                    if (splitLine[3] != "-")
                                    {
                                        // knoten hat mehrere vorgänger
                                        if (splitLine[3].Contains(','))
                                        {

                                            // vorgänger split
                                            string[] vorgänger = splitLine[3].Split(',');

                                            // jeden vorgänger eintragen
                                            foreach (string item in vorgänger)
                                            {
                                                try
                                                {
                                                    neuerKnoten.vorgänger.Add(network.listeVorgänge.Where(ne => ne.vorgang == item).Single());
                                                }
                                                catch (Exception ex)
                                                {
                                                    // fehler vorgänger
                                                    network.errorStack.Push( string.Format("vorgang nr. {0}: vorgänger fehlerhaft", lineCounter));
                                                    network.errorStack.Push(ex.Message);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            // knoten hat einen vorgänger
                                            string vorgänger = splitLine[3];

                                            // vorgänger eintragen
                                            try
                                            {
                                                neuerKnoten.vorgänger.Add(network.listeVorgänge.Where(ne => ne.vorgang == vorgänger).Single());
                                            }
                                            catch (Exception ex)
                                            {
                                                // fehler vorgänger
                                                network.errorStack.Push(string.Format("vorgang nr. {0}: vorgänger fehlerhaft", lineCounter));
                                                network.errorStack.Push(ex.Message);
                                            }
                                        }
                                    }

                                    // knoten in netzwerk eintragen
                                    network.listeVorgänge.Add(neuerKnoten);
                                }
                                else
                                {
                                    // fehler zuviel oder zuwenig attribute
                                    network.errorStack.Push(string.Format("vorgang nr. {0}: eintrag benötigt genau 4 attribute", lineCounter));
                                }
                            }
                            else
                            {
                                // fehler kein ; gefunden
                                network.errorStack.Push(string.Format("vorgang nr. {0}: eintrag muss mit ';' getrennt werden", lineCounter));
                            }

                            // eintrags zähler
                            lineCounter++;

                        }
                    }
                }
            }
            #endregion

            #region nachfolger einfügen
            // jeder knoten
            foreach (NetworkEntry aktuellerKnoten in network.listeVorgänge)
            {

                // jeder vorgänger
                foreach (NetworkEntry vorgängerKnoten in aktuellerKnoten.vorgänger)
                {
                    // nachfolger eintragen
                    vorgängerKnoten.nachfolger.Add(aktuellerKnoten);
                }
            }
            #endregion

            #region start und ende validieren
            // hat genau einen startpunkt
            if (network.listeVorgänge.Where(e => e.vorgänger.Count == 0).Count() != 1)
            {
                network.errorStack.Push("keiner oder mehfacher startpunkt");
            }

            // hat genau einen endpunkt
            if (network.listeVorgänge.Where(e => e.nachfolger.Count == 0).Count() != 1)
            {
                network.errorStack.Push("keiner oder mehfacher endpunkt");
            }
            #endregion

        }

        public void Write(string outputPath)
        {

        }

        public void GetHelp()
        {

        }

    }
}
