﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetzwerkplanCreator
{
    class NetworkEntry
    {
        public string beschreibung;
        public int dauer;
        public string vorgang;

        public int faz;
        public int fez;
        public int saz;
        public int sez;
        public int fp;
        public int gp;

        public List<NetworkEntry> vorgänger = new List<NetworkEntry>();
        public List<NetworkEntry> nachfolger = new List<NetworkEntry>();



        public NetworkEntry(string vorgang, string beschreibung, int dauer)
        {
            this.beschreibung = beschreibung;
            this.dauer = dauer;
            this.vorgang = vorgang;
        }


        // 1. frühester anfangszeitpunkt
        public int GET_FAZ()
        {
            // default result ist 0
            int result = 0;

            // hat vorgänger oder ist erster knotenpunkt 
            if (vorgänger.Count > 0)
            {
                // größter fez aller vorgänger
                result = this.vorgänger.Max(v => v.GET_FEZ());
            }

            // ergebnis faz
            return result;
        }


        // 2. frühester endzeitpunkt
        public int GET_FEZ()
        {
            // fez = faz + dauer
            int result = GET_FAZ() + dauer;

            // ergebnis fez
            return result;
        }
        

        // spätester endzeitpunkt
        public int GET_SEZ()
        {
            // default result ist 0
            int result = 0;

            // nachfolger zählen
            if (nachfolger.Count > 0)
            {
                // kleinster saz aller vorgänger
                result = nachfolger.Min(e => e.GET_SAZ());
            }
            else
            {
                // ist letzter eintrag daher sez = fez
                result = this.GET_FEZ();
            }

            return result;
        }


        // spätester anfangszeitpunkt
        public int GET_SAZ()
        {
            // saz = sez - dauer
            int result = 0;

            // saz ist sez - dauer
            result = this.GET_SEZ() - this.dauer;

            // ergebnis saz
            return result;
        }


        // gesamtpuffer
        public int GET_GP()
        {
            // gp = sez - fez
            int result = GET_SEZ() - GET_FEZ();

            // ergebnis gp
            return result;
        }


        // freier puffer
        public int GET_FP()
        {
            this.gp = GET_GP();

            // default result ist 0
            int result = 0;

            // hat vorgänger oder ist erster knotenpunkt 
            if (vorgänger.Count > 0)
            {
                // kleinster faz aller vorgänger
                result = this.vorgänger.Min(v => v.GET_FAZ() - this.GET_FEZ());
            }
            else
            {
                result = this.GET_GP();
            }

            // ergebnis fp            
            return result;
        }
    }
}