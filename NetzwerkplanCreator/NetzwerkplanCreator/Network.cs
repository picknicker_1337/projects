﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetzwerkplanCreator
{
    class Network
    {
        public int errorCounter = 0;

        public Stack<string> errorStack = new Stack<string>();

        public List<NetworkEntry> listeVorgänge = new List<NetworkEntry>();

        // ToDo InitNetwork
        public void InitNetwork()
        {
            if (errorStack.Count > 0)
            {
                Console.WriteLine("initialisierung nicht möglich..");
                foreach (string item in errorStack)
                {
                    Console.WriteLine(item);
                }
            }
            else
            {

                foreach (NetworkEntry entry in listeVorgänge)
                {
                    // frühester anfangszeitpunkt
                    entry.faz = entry.GET_FAZ();

                    // frühester endzeitpunkt
                    entry.fez = entry.GET_FEZ();

                    // freier puffer
                    entry.fp = entry.GET_FP();
                }

                // liste reihenfolge umkehren
                listeVorgänge.Reverse();


                foreach (NetworkEntry entry in listeVorgänge)
                {
                    // spätester endzeitpunkt
                    entry.sez = entry.GET_SEZ();

                    // spätester anfangszeitpunkt
                    entry.saz = entry.GET_SAZ();

                }

                // liste reihenfolge original
                listeVorgänge.Reverse();

                foreach (NetworkEntry entry in listeVorgänge)
                {
                    // gesamtpuffer
                    entry.gp = entry.GET_GP();

                    // freier puffer
                    entry.fp = entry.GET_FP();
                }
            }
        }
    }
}
