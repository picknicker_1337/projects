﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SOAP_NorthwindDB_Client_CustomerAndOrder.ServiceReference;

namespace SOAP_NorthwindDB_Client_CustomerAndOrder
{
    class Program
    {
        static void Main(string[] args)
        {
            WebServiceSoapClient client = new WebServiceSoapClient();

            Console.WriteLine("1. customer by customerID\n"
                              + "2. customer by country\n"
                              + "3. order by customerID\n"
                              + "4. order by orderID\n");

            string userInput = Console.ReadLine();

            switch (userInput)
            {
                case "1":
                    Console.Write("customerID eintragen: ");

                    CustomerInfo resultCustomer = client.GetCustomerByID(Console.ReadLine());

                    Console.WriteLine("{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}\n{8}\n{9}",
                    resultCustomer.CustomerID,
                    resultCustomer.CompanyName,
                    resultCustomer.ContactTitle,
                    resultCustomer.Address,
                    resultCustomer.City,
                    resultCustomer.Region,
                    resultCustomer.PostalCode,
                    resultCustomer.Country,
                    resultCustomer.Phone,
                    resultCustomer.Fax
                    );
                    break;

                case "2":
                    Console.Write("country eintragen: ");
                    CustomerInfo[] resultCustomerArray = client.GetCustomerByCountry(Console.ReadLine());
                    foreach (var item in resultCustomerArray)
                    {
                        Console.WriteLine("{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}\n{8}\n{9}",
                        item.CustomerID,
                        item.CompanyName,
                        item.ContactTitle,
                        item.Address,
                        item.City,
                        item.Region,
                        item.PostalCode,
                        item.Country,
                        item.Phone,
                        item.Fax
                        );
                    }
                    break;

                case "3":
                    Console.Write("OrderID eintragen: ");
                    OrderInfo[] resultOrderArray = client.GetOrdersByCustomerID(Console.ReadLine());
                    foreach (var item in resultOrderArray)
                    {
                        Console.WriteLine("{0}\n{1}\n{2}\n{3}\n{4}",
                        item.OrderID,
                        item.CustomerID,
                        item.OrderDate,
                        item.RequiredDate,
                        item.ShippedDate
                        );
                    }
                    break;

                case "4":
                    Console.Write("OrderID eintragen: ");
                    int orderID = Convert.ToInt32(Console.ReadLine());
                    decimal result = client.GetOrderValueByID(orderID);
                    Console.WriteLine("Summe: "+ result);
                    break;

                default:
                    Console.WriteLine("keine Einträge gefunden..");
                    break;
            }
            Console.ReadKey();
        }
    }
}
