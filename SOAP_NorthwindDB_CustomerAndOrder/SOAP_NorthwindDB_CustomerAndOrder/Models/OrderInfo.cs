﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOAP_NorthwindDB_CustomerAndOrder.Models
{
    public class OrderInfo
    {
        public string OrderID { get; set; }
        public string CustomerID { get; set; }
        public string OrderDate { get; set; }
        public string RequiredDate { get; set; }
        public string ShippedDate { get; set; }
    }
}