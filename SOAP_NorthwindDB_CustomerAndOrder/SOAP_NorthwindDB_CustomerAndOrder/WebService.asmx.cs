﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using SOAP_NorthwindDB_CustomerAndOrder.Models;
using System.Data.SqlClient;
using System.Diagnostics;

namespace SOAP_NorthwindDB_CustomerAndOrder
{
    /// <summary>
    /// Zusammenfassungsbeschreibung für WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Wenn der Aufruf dieses Webdiensts aus einem Skript zulässig sein soll, heben Sie mithilfe von ASP.NET AJAX die Kommentarmarkierung für die folgende Zeile auf. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        string connectionString = @"Data Source=.\SQLEXPRESS;" +
                                   "Initial Catalog=Northwind;" +
                                   "Integrated Security=true;";
        
        [WebMethod]
        public CustomerInfo GetCustomerByID(string customerID)
        {
            CustomerInfo result = new CustomerInfo();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("SELECT CustomerID, CompanyName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax FROM Customers WHERE CustomerID = '" + customerID + "'", connection);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        result.CustomerID = reader.GetString(0);
                        result.CompanyName = reader.GetString(1);
                        result.ContactTitle = reader.GetString(2);
                        result.Address = reader.GetString(3);
                        result.City = reader.GetString(4);
                        result.Region = reader.GetString(5);
                        result.PostalCode = reader.GetString(6);
                        result.Country = reader.GetString(7);
                        result.Phone = reader.GetString(8);
                        result.Fax = reader.GetString(9);
                    }

                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }

                return result;
            }
        }

        [WebMethod]
        public CustomerInfo[] GetCustomerByCountry(string country)
        {
            List<CustomerInfo> resultList = new List<CustomerInfo>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("SELECT CustomerID, CompanyName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax FROM Customers WHERE Country = '" + country +"';", connection);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        CustomerInfo result = new CustomerInfo();

                        

                        result.CustomerID = reader.GetString(0);
                        result.CompanyName = reader.GetString(1);
                        result.ContactTitle = reader.GetString(2);
                        result.Address = reader.GetString(3);
                        result.City = reader.GetString(4);

                        if (reader.IsDBNull(5) == false)
                        {
                            result.Region = reader.GetString(5);
                        }
                        
                        result.PostalCode = reader.GetString(6);
                        result.Country = reader.GetString(7);
                        result.Phone = reader.GetString(8);

                        if (reader.IsDBNull(9) == false)
                        {
                            result.Fax = reader.GetString(9);
                        }

                        resultList.Add(result);
                    }

                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }

                CustomerInfo[] resultArray = resultList.ToArray();

                return resultArray;
            }
        }

        [WebMethod]
        public OrderInfo[] GetOrdersByCustomerID(string customerID)
        {
            List<OrderInfo> resultList = new List<OrderInfo>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("SELECT OrderID, CustomerID, OrderDate, RequiredDate, ShippedDate FROM Orders WHERE CustomerID = '" + customerID + "';", connection);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        OrderInfo result = new OrderInfo();

                        result.OrderID = Convert.ToString(reader.GetInt32(0));
                        result.CustomerID = reader.GetString(1);
                        result.OrderDate = Convert.ToString( reader.GetDateTime(2));
                        result.RequiredDate = Convert.ToString(reader.GetDateTime(3));
                        result.ShippedDate = Convert.ToString(reader.GetDateTime(4));
                        
                        resultList.Add(result);
                    }

                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }

                OrderInfo[] resultArray = resultList.ToArray();

                return resultArray;
            }
        }

        [WebMethod]
        public decimal GetOrderValueByID(int orderID)
        {
            decimal result = 0;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("SELECT UnitPrice, Quantity FROM [Order Details] WHERE OrderID = '" + orderID + "';", connection);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        decimal tempResult = 0;

                        tempResult += reader.GetDecimal(0);
                       tempResult = tempResult * reader.GetInt16(1);

                        result += tempResult;
                    }

                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }

                return result;
            }
        }
    }
}
