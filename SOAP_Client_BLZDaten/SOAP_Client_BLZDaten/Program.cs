﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SOAP_Client_BLZDaten.LocalBLZService;


namespace SOAP_Client_BLZDaten
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Zu prüfende BLZ eintragen: ");

            string blz = Console.ReadLine();

            LocalBLZServiceSoapClient client = new LocalBLZServiceSoapClient();
            
            BankInfo bankInfo = client.GetBankByBlz(blz);

            Console.WriteLine(" \nBezeichnung: {0} \nOrt: {1} \nPlz: {2}",bankInfo.Bezeichnung
                                            ,bankInfo.Ort
                                            ,bankInfo.Plz);
            Console.ReadKey();
        }
    }
}
