﻿using SOAP_Dienst_BLZDaten.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace SOAP_Dienst_BLZDaten
{
    /// <summary>
    /// Zusammenfassungsbeschreibung für LocalBLZService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Wenn der Aufruf dieses Webdiensts aus einem Skript zulässig sein soll, heben Sie mithilfe von ASP.NET AJAX die Kommentarmarkierung für die folgende Zeile auf. 
    // [System.Web.Script.Services.ScriptService]
    public class LocalBLZService : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public BankInfo GetBankByBlz(string blz)
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;" +
                                   "Initial Catalog=TeachSQL;" +
                                   "Integrated Security=true;";


            BankInfo result = new BankInfo();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("SELECT Bezeichnung, Ort, PLZ FROM BLZTabelle WHERE BLZ = @gesuchteBLZ", connection);
                    command.Parameters.AddWithValue("gesuchteBLZ", blz);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Bezeichnung = reader.GetString(0);
                        result.Ort = reader.GetString(1);
                        result.Plz = Convert.ToString(reader.GetInt32(2));

                    }

                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }

            return result;
        }


    }
}
