﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOAP_Dienst_BLZDaten.Models
{
    public class BankInfo
    {
        public string Bezeichnung { get; set; }
        public string Ort { get; set; }
        public string Plz { get; set; }

        public BankInfo()
        {

        }        
    }
}